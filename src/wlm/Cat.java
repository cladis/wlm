package wlm;

import java.io.BufferedReader;
import java.io.*;
import java.util.HashMap;
import net.wikipedia.Wiki;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class Cat {

    public static String допустимі_ідентифікатори = "";
    public static String розділювач_початку = " , <";
    public static String розділювач_кінця = ">";
    public static String усі_коатуу = "9999999999";
    public static String усі_категорії = "";

    public static HashMap<String, String> карта_муніципалітетів = new HashMap<>();
    public static HashMap<String, String> карта_объєктів = new HashMap<>();

    public static void main(String[] args) throws Exception {
        Wiki w = new Wiki("uk.wikipedia.org");
        w.setUserAgent("WPBot 1.0");
        w.login(args[0], args[1]);
        w.setMarkBot(true);
        w.setMarkMinor(true);

        Wiki com = new Wiki("commons.wikimedia.org");
        com.setUserAgent("WPBot 1.0");
        com.login(args[0], args[1]);
        com.setMarkBot(true);
        com.setMarkMinor(true);

        File f = new File("e:\\downloads\\КОАТУУ 5 значущих цифр — відповідності категоріям пам'яток на Вікісховищі - Sheet1.tsv");
        f.setWritable(true);
        f.setReadable(true);
        System.out.println(f.getTotalSpace());
        BufferedReader in = new BufferedReader(new FileReader(f));
        String таблиця_коатуу = "";
        int i = 0;
        while (in.ready()) {
            String рядок = in.readLine();
            таблиця_коатуу += рядок + "\n";
            i++;
            if (i > 1) {//пропускаємо заголовок таблиці
                опрацювати_рядок(w, рядок);
            }
        }
        //System.out.println("ПОЧАТОК\n" + таблиця_коатуу + "КІНЕЦЬ");

        String[] списки = отримати_списки(w);

        for (int j = 0; j < списки.length; j++) {
            String список = списки[j];
            опрацювати_список(w, список);
        }

        String[] файли = отримати_файли(com);

        for (int j = 0; j < файли.length; j++) {

            String файл = файли[j];

            опрацювати_файл(com, файл);

        }

//        String[] коатуу = усі_коатуу.split("\\|");
//        for (int k = 1; k < коатуу.length; k++) {
//            if (карта_муніципалітетів.containsKey(коатуу[k])) {
//                System.out.println("тест\tідентифікатор:\t" + коатуу[k] + "\tкатегорія:\t" + карта_муніципалітетів.get(коатуу[k]));
//            } else {
//                System.out.println("ІДЕНТИФІКАТОР\t" + коатуу[k] + "\t— НЕМОЖЛИВИЙ");
//            }
//        }
    }

    public static String[] отримати_списки(Wiki w) throws Exception {
        String[] списки = w.whatTranscludesHere("Шаблон:ВЛП-рядок", 4);
        return списки;
    }

    public static void опрацювати_список(Wiki w, String список) throws Exception {
        String текст_списку = w.getPageText(список);
        String[] елементи_списку = текст_списку.split("\\{\\{\\s*[Вв]ЛП-рядок");
        for (int i = 1; i < елементи_списку.length; i++) {
            String елемент_списку = елементи_списку[i];

            елемент_списку = елемент_списку.replaceAll("\\|\\s*", "|");
            int a = елемент_списку.indexOf("|ID");
            int b = елемент_списку.indexOf("=", a) + 1;
            int c = елемент_списку.indexOf("|", b);
            String ідентифікатор = елемент_списку.substring(b, c).trim();

            допустимі_ідентифікатори += допустимі_ідентифікатори.contains(огорнути(ідентифікатор)) ? "" : огорнути(ідентифікатор);

            if (!карта_муніципалітетів.containsKey(перетворити_ідентифікатор(ідентифікатор))) {

                System.out.println("ІДЕНТИФІКАТОР\t" + ідентифікатор + "\t— НЕМОЖЛИВИЙ");
            } else {

                a = елемент_списку.indexOf("|галерея");
                b = елемент_списку.indexOf("=", a) + 1;
                c = елемент_списку.indexOf("|", b);
                int d = елемент_списку.indexOf("}}", b);
                c = (c == -1 || (d < c && d != -1)) ? d : c;
                String галерея = елемент_списку.substring(b, c).replaceAll("_", " ").trim();

                if (!карта_объєктів.containsKey(ідентифікатор)) {
                    карта_объєктів.put(ідентифікатор, галерея);
                }
            }
        }
    }

    /**
     * опрацьовує рядок таблиці відповідності коатуу категоріям
     *
     * @param w
     * @param ряд
     * @throws Exception
     */
    public static void опрацювати_рядок(Wiki w, String ряд) throws Exception {
        String рядок = ряд;
        String[] елементи_рядка = рядок.split("\t");
        if (елементи_рядка.length < 4) {
            return;
        }
        String ідентифікатор = елементи_рядка[0];
        ідентифікатор = (ідентифікатор.length() == 9) ? 0 + ідентифікатор : ідентифікатор;
        //System.out.println(ідентифікатор);
        усі_коатуу += "|" + ідентифікатор;
        String категорія = елементи_рядка[3];
        //System.out.println((елементи_рядка.length-1)+"\t"+категорія);
        карта_муніципалітетів.put(ідентифікатор, категорія);

        if (усі_категорії.contains(огорнути(категорія))) {
            System.out.println("ПОВТОР КАТЕГОРІЇ У ТАБЛИЦІ: " + категорія);
        } else {
            усі_категорії += огорнути(категорія);
        }

    }

    /**
     * Огортає шматок тексту розділювачами початку та кінця
     *
     * @param шматок_тексту
     * @return
     */
    public static String огорнути(String шматок_тексту) {
        return розділювач_початку + шматок_тексту + розділювач_кінця;
    }

    /**
     * Перетворює конкурсний ідентифікатор на коатуу з 5-ма значущими цифрами
     *
     * @param ідентифікатор
     * @return
     */
    public static String перетворити_ідентифікатор(String ідентифікатор) {
        return ідентифікатор.replaceAll(".*([0-9]{2})-([0-9]{3})-[0-9]{4}.*", "$1$200000");
    }

    public static String[] отримати_файли(Wiki com) throws Exception {
        String[] а = com.whatTranscludesHere("Template:Monument Ukraine", 6);
        String[] б = com.getCategoryMembers("Cultural heritage monuments in Ukraine with incorrect IDs", 6);
        String[] в = com.getCategoryMembers("Cultural heritage monuments in Ukraine", 6);
        String[] файли = Wiki.intersection(в, Wiki.relativeComplement(а, б));
        return файли;
    }

    public static void опрацювати_файл(Wiki com, String файл) throws Exception {
        try {

            String[] категорії_файлу = com.getCategories(файл);
            String перелік_категорій_файлу = "";
            for (int i = 0; i < категорії_файлу.length; i++) {
                String існуюча_категорія = категорії_файлу[i];
                перелік_категорій_файлу += огорнути(існуюча_категорія);
                if (карта_объєктів.containsValue(існуюча_категорія)) {
                    System.out.println("Файл " + файл + " вже містить категорію об'єкту " + існуюча_категорія);

                    //TEMP
                    String текст_файлу = com.getPageText(файл);
                    текст_файлу = текст_файлу.replaceAll(
                            "\\[\\[\\s*[Cc]ategory\\s*\\:\\s*" 
                            + "[Cc]ultural heritage monuments in Ukraine" 
                            + "((\\|[^\\|^]]]])|]])",
                            ""
                    );
                    com.edit(файл, текст_файлу, "-[[Category:Cultural heritage monuments in Ukraine]]");

                    //END TEMP
                    return;
                }
            }
            String текст_файлу = com.getPageText(файл);
            //{{Monument Ukraine|80-382-0200}}
            String[] шматки_тексту_файлу = текст_файлу.split("\\{\\{\\s*[Mm]onument[ _]Ukraine");
            String додати_категорії = "";
            for (int i = 1; i < шматки_тексту_файлу.length; i++) {
                String шматок_тексту_файлу = шматки_тексту_файлу[i];
                int a = шматок_тексту_файлу.indexOf("|") + 1;
                int b = шматок_тексту_файлу.indexOf("}}", a);
                if (a == b) {
                    System.out.println("ФАЙЛ\t" + файл + "\tМІСТИТЬ ПОРОЖНІЙ ІДЕНТИФІКАТОР");
                    continue;
                }

                String ідентифікатор = шматок_тексту_файлу.substring(a, b).trim();
                if (!допустимі_ідентифікатори.contains(огорнути(ідентифікатор))) {
                    System.out.println("ФАЙЛ\t" + файл + "\tМІСТИТЬ НЕДОПУСТИМИЙ ІДЕНТИФІКАТОР\t" + ідентифікатор);
                    return;
                }
                String нова_категорія = "Cultural heritage monuments in Ukraine";
                if (!"".equals(карта_объєктів.get(ідентифікатор))) {
                    нова_категорія = карта_объєктів.get(ідентифікатор);
                } else {
                    String перетворений_ідентифікатор = перетворити_ідентифікатор(ідентифікатор);
                    if (!карта_муніципалітетів.containsKey(перетворений_ідентифікатор)) {
                        //1234500000
                        перетворений_ідентифікатор = перетворений_ідентифікатор.replaceAll("([0-9]{3})[0-9]{7}", "$10000000");
                        if (!карта_муніципалітетів.containsKey(перетворений_ідентифікатор)) {
                            перетворений_ідентифікатор = перетворений_ідентифікатор.replaceAll("([0-9]{2})[0-9]{8}", "$100000000");
                        }
                    }
                    нова_категорія = карта_муніципалітетів.get(перетворений_ідентифікатор);
                }

                if (перелік_категорій_файлу.contains(огорнути(нова_категорія))) {
                    System.out.println("Файл " + файл + " вже містить категорію муніципалітету " + нова_категорія);
                    if (i == шматки_тексту_файлу.length - 1) {
                        return;
                    }
                } else {
                    додати_категорії = ("".equals(додати_категорії)) ? "[[Category:" + нова_категорія + "]]" : "\n[[Category:" + нова_категорія + "]]";
                }
            }//кінець циклу

            //System.out.println("ТЕКСТ ФАЙЛУ ДО\n" + текст_файлу);
            for (int i = 0; i < категорії_файлу.length; i++) {
                String існуюча_категорія = категорії_файлу[i];
                if (карта_муніципалітетів.containsValue(існуюча_категорія)) {
                    System.out.println("Файл\t" + файл + "\tмістить категорію\t" + існуюча_категорія + "t, оскільки вона не завадила, то буде додано категорію нижчого рівня, а цю — вилучено");
                    текст_файлу = текст_файлу.replaceAll("\\[\\[\\s*[Cc]ategory\\s*\\:\\s*" + існуюча_категорія + "((\\|[^\\|^]]]])|]])", "");
                }
            }
            текст_файлу = текст_файлу.replaceAll("\\[\\[\\s*[Cc]ategory\\s*\\:\\s*" + "[Cc]ultural heritage monuments in Ukraine" + "((\\|[^\\|^]]]])|]])", "");

            текст_файлу += "\n" + додати_категорії;
            //System.out.println("ТЕКСТ ФАЙЛУ ПІСЛЯ\n" + текст_файлу);

            com.edit(файл, текст_файлу, "+" + додати_категорії.replaceAll("\n", " ").trim() + " Уточнено категорію на основі ID ВЛП. Clarified category basing on WLM ID.");
        } catch (java.io.IOException e) {
            опрацювати_файл(com, файл);
        }
    }

    /**
     * Перевіряє чи сторінка категорії є перенаправленням, або містить шаблон
     * Категорії-перенаправлення. У разі позитивної перевірки повертає цільову
     * категорію.
     *
     * @param w
     * @param кіт
     * @return
     */
    public static String перенаправлення_категорії(Wiki com, String кіт) {
        String категорія = кіт;

        return null;
    }
}
