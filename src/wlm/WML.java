/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wlm;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import net.wikipedia.Wiki;
import net.wikipedia.Wiki.LogEntry;

/**
 *
 * @author Base <base-w at yandex.ru>
 */
public class WML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        Wiki w = new Wiki("uk.wikipedia.org");
//        Wiki w = new Wiki("localhost/basewiki");
//        w.setScriptPath("");
//        w.setUsingCompressedRequests(false);
//        w.setUserAgent("WPBot 1.0");
//        w.login(args[0], args[1]);
//        w.setMarkBot(true);
//        w.setMarkMinor(true);

        Wiki com = new Wiki("commons.wikimedia.org");
        
        com.setUserAgent("WPBot 1.0");
        com.login(args[0], args[1]);
        com.setMarkBot(true);
        com.setMarkMinor(true);

        String[] files = com.extUrlUsage("tools.wmflabs.org/heritage/api/api.php?action=search&format=html&srcountry=ua&srlang=uk&srid=99-999-9999&props=image%7Cname%7Caddress%7Cmunicipality%7Clat%7Clon%7Cid%7Ccountry%7Csource%7Cmonument_article%7Cregistrant_url", "6");
        if (files == null) {
            System.exit(10);//no files
        }
        int fl=files.length;
         System.out.println(fl);//<b>f</b>iles <b>l</b>enght
        for (int i = 0; i < fl; i++) {
            String file = files[i];
            System.out.println(i + "\t" + file);
            String id = file.replaceAll("File\\:([0-9]{2}-[0-9]{3}-[0-9]{4}).*", "$1");
            System.out.println(i + "\t" + id);
            String filetext = com.getPageText(file);
            String desc = com.WLMGetDesc(id);
            if (desc == null) {
                System.exit(11);//wrong id or labs's down
            }
            System.out.println(i + "\t" + desc);
            filetext=filetext.replaceAll("\\{\\{[uU]k\\|1=[^}]*}}", "{{uk|1="+desc+"}}");
            filetext=filetext.replaceAll("\\{\\{[Mm]onument Ukraine\\|99-999-9999}}", "{{Monument Ukraine|"+id+"}}");
        
            System.out.println(filetext);
            com.edit(file, filetext, "WLM-UA maintaince: Заповнення опису файлу за даним у назві ідентифікатором");
        }
    }

}
